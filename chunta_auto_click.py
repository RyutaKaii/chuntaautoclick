import wx
import cv2
import threading
import traceback
import os
import sys
import time
import datetime

# クリック処理
import pyautogui

# 画面キャプチャ処理
import win32api
import win32con
import win32ui
import win32gui
from PIL import ImageGrab
from PIL import Image
import numpy as np

# OCR処理
import pyocr
import pyocr.builders

# リンク処理
import webbrowser





# ==================================================
# ==================================================
# Chunta Auto Click GUI
#   
#   Chunta Auto Click用GUIを提供
#   
#   [参考URL]
#   ・pythonで業務効率化／RPA自作 – 検討 その５ マルチモニタ環境での画面全体の取得方法
#     https://hellobreak.net/opencv-mouse-0304/
#   ・人跡既踏
#     https://amdkkj.blogspot.com/2017/06/converting-opencv-python-images-into-wxpython-images_17.html
# ==================================================
# ==================================================

# ==================================================
# ==================================================
# Chunta Auto Click
# 
#   windows上で指定された領域内の指定されたテキスト部分をクリックします。
#   
#   [処理概要]
#     1. 指定された領域内で指定されたテキストをOCRで検索
#     2. 発見した場合、テキスト部分をクリックする
# 
#   [前準備]
#     このコードを実行するには下記2つのツールのインストールが必要です。
#     ※pip installでは利用できません。
#       ・Tesseract OCR
#       ・win32gui()
# 
#   [参考URL]
#   ・pythonで業務効率化／RPA自作 – 検討 その５ マルチモニタ環境での画面全体の取得方法
#     https://se.yuttar-ixm.com/multi-monitor-cap/
#   ・【Python】Pillow ↔ OpenCV 変換
#     https://qiita.com/derodero24/items/f22c22b22451609908ee
#   ・Pythonで定周期で実行する方法と検証
#     https://qiita.com/montblanc18/items/05715730d99d450fd0d3
#   ・字幕から文字抽出してみた (OpenCV:tesseract-ocr編)
#     https://qiita.com/satsukiya/items/112c6c08a618b2376408
#   ・Tesseract OCR をWindowsにインストールする方法
#     https://gammasoft.jp/blog/tesseract-ocr-install-on-windows/
#   ・Pythonでクリックを自動化した
#     https://qiita.com/mkdb/items/f5e09b9ec3ea77de016d
#   ・Python datetime 日付の計算、文字列変換をする方法 strftime, strptime【決定版】
#     https://qiita.com/7110/items/4ece0ce9be0ce910ee90
# ==================================================
# ==================================================

# ==================================================
# ==================================================
# exe化方法
# 
# pyinstallerを利用してexe化します。
# 起動時間の関係で複数ファイル構成で作成します。
# 
# 1. コマンドプロンプトで下記コマンドを実行
#      pyinstaller --clean --icon=chunta_auto_click.ico -n chunta_auto_click chunta_auto_click.py --noconsole
# 2. 作成されたdist/chunta_auto_clickのフォルダ内にresourceフォルダを配置
# 3. 作成されたdist/chunta_auto_clickのフォルダを配布
# ==================================================
# ==================================================





# ==================================================
# ==================================================
# 
# Chunta Auto Click GUI
# 
# ==================================================
# ==================================================

# ==================================================
# 定数
# ==================================================
# 初期値 テキスト
INIT_TXT = '広告を'

# 初期値 最小時間間隔(秒)
INIT_MIN_INTERVAL_TIME = 1

# 初期値 座標
INIT_ZAHYO = -1

# フレーム タイトル
FLAME_TITLE = 'Chunta Auto Click'

# フレーム 横幅
FLAME_WIDTH = 200

# フレーム 縦幅
FLAME_HEIGHT = 220

# ステータスボタン 横幅
STAT_BTN_WIDTH = 200

# ステータスボタン 縦幅
STAT_BTN_HEIGHT = 80

# ステータスボタン START 色
STAT_BTN_COLOR_START = '#00FF00'

# ステータスボタン STOPPING 色
STAT_BTN_COLOR_STOPPING = '#808080'

# ステータスボタン STOP 色
STAT_BTN_COLOR_STOP = '#FF0000'

# 設定ボタン 横幅
SETTING_BTN_WIDTH = 200

# 設定ボタン 縦幅
SETTING_BTN_HEIGHT = 40

# 画面キャプチャ画像 縦幅
CAPTURE_IMG_WIDTH = 800

# ステータス切り替え最小時間(秒)
MIN_TIME_STAT_CHANGE = 5

# ヘルプボタンリンク URL
URL_HELP = 'http://chunta.html.xdomain.jp/index.html'

# ==================================================
# グローバル変数
# ==================================================
# 実行スレッド
exec_thread = None

# 設定値クラス
setting_value = None

# ==================================================
# 設定値
# ==================================================
class SETTING_VALUE:
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self):
        # 停止フラグ
        self.flg_stop = True
        
        # 検索テキスト
        self.txt = INIT_TXT
        
        # 最小実行間隔(秒)
        self.min_interval_time = INIT_MIN_INTERVAL_TIME
        
        # top
        self.top = INIT_ZAHYO
        
        # bottom
        self.bottom = INIT_ZAHYO
        
        # left
        self.left = INIT_ZAHYO
        
        # right
        self.right = INIT_ZAHYO
        
        # rate
        self.rate = 1

# ==================================================
# メインウインドウ
# ==================================================
class WINDOW_MAIN:
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self, title, size_x, size_y):
        # タイトル
        self.title = title
        # サイズ x
        self.size_x = size_x
        # サイズ y
        self.size_y = size_y
        
        # フレーム
        self.frame = wx.Frame(None, wx.ID_ANY, title, size=(size_x, size_y))
        
        # パネル root
        self.panel_root = wx.Panel(self.frame, wx.ID_ANY)
        # パネル ボタン
        self.panel_btn = PANEL_BTN(self.panel_root)
        
        # レイアウト設定
        layout_root = wx.BoxSizer(wx.VERTICAL)
        layout_root.Add(self.panel_btn, 0, wx.GROW | wx.BOTTOM | wx.TOP)
        self.panel_root.SetSizer(layout_root)
        layout_root.Fit(self.panel_root)
    
    # =========================
    # 表示
    # =========================
    def show(self):
        self.frame.Show()

# ==================================================
# パネル ボタン
# ==================================================
class PANEL_BTN(wx.Panel):
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self, parent):
        super().__init__(parent, wx.ID_ANY)
        
        # ラベル
        self.label = wx.StaticText(self, wx.ID_ANY, 'ChuntaAutoClick')
        font_label = wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        self.label.SetFont(font_label)
        
        # ステータスボタン
        self.stat_btn = wx.Button(self, wx.ID_ANY, 'EXECUTE', size=(STAT_BTN_WIDTH, STAT_BTN_HEIGHT))
        self.stat_btn.SetBackgroundColour(STAT_BTN_COLOR_START)
        self.stat_btn.Bind(wx.EVT_BUTTON, self.onclick_stat_btn)
        
        # ウインドウ設定ボタン
        self.window_btn = wx.Button(self, wx.ID_ANY, 'SETTING', size=(SETTING_BTN_WIDTH, SETTING_BTN_HEIGHT))
        self.window_btn.Bind(wx.EVT_BUTTON, self.onclick_window_btn)
        
        # ヘルプリンク
        self.label_help = wx.StaticText(self, wx.ID_ANY, 'Presented by ChuntaWeb')
        font_label_help = wx.Font(10, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        self.label_help.SetFont(font_label_help)
        self.label_help.Bind(wx.EVT_MOUSE_EVENTS, self.event_label_help)
        
        # 描画中フラグ
        self.flg_drawing = False
        # 開始位置
        self.ix, self.iy = 0, 0
        
        layout = wx.BoxSizer(wx.VERTICAL)
        layout.Add(self.label, flag = wx.EXPAND | wx.TOP | wx.BOTTOM | wx.LEFT, border=5)
        layout.Add(self.stat_btn, flag = wx.EXPAND | wx.BOTTOM, border=5)
        layout.Add(self.window_btn, flag = wx.EXPAND | wx.BOTTOM, border=5)
        layout.Add(self.label_help, flag=wx.ALIGN_RIGHT)
        
        self.SetSizer(layout)
    
    # =========================
    # ステータスボタンクリック時処理
    # =========================
    def onclick_stat_btn(self, event):
        global setting_value
        
        if INIT_ZAHYO == setting_value.top:
            wx.MessageBox('First select the area with the setting button.\n最初に設定ボタンで領域を選択してください', 'Error エラー')
            return
        
        if not setting_value.flg_stop:
            self.stat_btn.Disable()
            self.stat_btn.SetBackgroundColour(STAT_BTN_COLOR_STOPPING)
            self.stat_btn.SetLabel('EXECUTE')
            
            stop()
            
            time.sleep(MIN_TIME_STAT_CHANGE)
            self.stat_btn.SetBackgroundColour(STAT_BTN_COLOR_START)
            self.stat_btn.Enable()
        else:
            self.stat_btn.Disable()
            self.stat_btn.SetBackgroundColour(STAT_BTN_COLOR_STOPPING)
            self.stat_btn.SetLabel('STOP')
            
            start()
            
            time.sleep(MIN_TIME_STAT_CHANGE)
            self.stat_btn.SetBackgroundColour(STAT_BTN_COLOR_STOP)
            self.stat_btn.Enable()
    
    # =========================
    # ウインドウ設定ボタンクリック時処理
    # =========================
    def onclick_window_btn(self, event):
        # 指定サイズの全画面キャプチャ画像取得
        self.img = get_capture_img(CAPTURE_IMG_WIDTH)
        
        # 表示用画像
        self.img_copy = None
        
        # window名設定
        cv2.namedWindow(winname='img')
        
        # マウスイベント設定
        cv2.setMouseCallback('img', self.draw_rectangle)
        
        # 画像表示
        cv2.imshow('img', self.img)
        
        wx.MessageBox('Choose About Where Your Ads Appear.\n広告が表示される場所をアバウトに選択してください', 'Select advertising area 広告領域を選択')
    
    # ==================================================
    # 四角形を描画
    # ==================================================
    def draw_rectangle(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.flg_drawing = True
            self.ix, self.iy = x, y
        
        elif event == cv2.EVENT_MOUSEMOVE:
            if self.flg_drawing == True:
                self.img_copy = self.img.copy()
                self.img_copy = cv2.rectangle(self.img_copy, (self.ix, self.iy), (x, y), (0, 0, 255), -1)
                cv2.imshow('img', self.img_copy)
        
        elif event == cv2.EVENT_LBUTTONUP:
            self.flg_drawing = False
            self.img_copy = cv2.rectangle(self.img_copy, (self.ix, self.iy), (x, y), (0, 0, 255), -1)
            cv2.imshow('img', self.img_copy)
        
        if event == cv2.EVENT_LBUTTONUP:
            global setting_value
            
            # 右に行くほど増加
            if self.ix < x:
                left = self.ix
                right = x
            else:
                left = x
                right = self.ix
            
            # 下に行くほど増加
            if self.iy < y:
                bottom = y
                top = self.iy
            else:
                bottom = self.iy
                top = y
            
            setting_value.top = top
            setting_value.bottom = bottom
            setting_value.left = left
            setting_value.right = right
    
    # ==================================================
    # ヘルプリンク押下時
    # ==================================================
    def event_label_help(self, event):
        if event.LeftUp():
            webbrowser.open(URL_HELP)
        
        event.Skip()

# ==================================================
# 指定サイズの全画面キャプチャ画像取得
# ==================================================
def get_capture_img(width):
    # レートを算出
    vscreenwidth = win32api.GetSystemMetrics(win32con.SM_CXVIRTUALSCREEN)
    vscreenx = win32api.GetSystemMetrics(win32con.SM_XVIRTUALSCREEN)
    desktop_width = vscreenx + vscreenwidth
    rate = width / desktop_width
    
    global setting_value
    setting_value.rate = rate
    
    # 全画面キャプチャ画像取得
    img = get_capture(False)
    
    # リサイズ後のサイズを算出
    orgHeight, orgWidth = img.shape[:2]
    width_resized = int(width)
    height_resized = int(orgHeight * rate)
    
    # リサイズ
    img_resized = cv2.resize(img , (width_resized, height_resized))
    
    return img_resized

# ==================================================
# auto_clickスレッド
# ==================================================
class auto_click_Thread(threading.Thread):
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self):
        super(auto_click_Thread, self).__init__()
        
        # 呼び出し元が終了したらスレッドも終了するようデーモン化
        self.setDaemon(True)
    
    # =========================
    # stop処理
    # =========================
    def stop(self):
        global setting_value
        
        setting_value.flg_stop = True
    
    # =========================
    # 実行処理
    # =========================
    def run(self):
        global setting_value
        
        setting_value.flg_stop = False
        
        txt = setting_value.txt
        min_interval_time = setting_value.min_interval_time
        top = int( setting_value.top * (1 / setting_value.rate) )
        bottom = int( setting_value.bottom * (1 / setting_value.rate) )
        left = int( setting_value.left * (1 / setting_value.rate) )
        right = int( setting_value.right * (1 / setting_value.rate) )
        
        click_text(txt, min_interval_time, top, bottom, left, right)

# ==================================================
# スタート
# ==================================================
def start():
    global exec_thread
    global setting_value
    
    exec_thread = auto_click_Thread()
    exec_thread.start()

# ==================================================
# ストップ
# ==================================================
def stop():
    global exec_thread
    
    exec_thread.stop()





# ==================================================
# ==================================================
# 
# Chunta Auto Click
# 
# ==================================================
# ==================================================

# ==================================================
# 定数
# ==================================================
# リソースフォルダ名
RESORSES_FOLDER_NAME = 'resources'

# ==================================================
# タイマー
# ==================================================
class TIMER:
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self, min_interval_time):
        # 最小実行時間
        self.min_interval_time = datetime.timedelta(seconds = min_interval_time)
        
        # 最終実行時間
        self.last_exec_time = datetime.datetime.now()
    
    # =========================
    # 時間の制限のため実行可能かを判定
    # =========================
    def can_exec(self):
        interval_time = datetime.datetime.now() - self.last_exec_time
        
        if interval_time < self.min_interval_time:
            return False
        
        return True

# ==================================================
# 座標
# ==================================================
class XY:
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self, x, y):
        # x
        self.x = x
        
        # y
        self.y = y
    
    # =========================
    # リセット
    # =========================
    def reset(self):
        self.x = -1
        self.y = -1

# ==================================================
# 文字列検索機
# ==================================================
class STR_FINDER:
    # =========================
    # コンストラクタ
    # =========================
    def __init__(self, find_str):
        # 検索文字列
        self.find_str = find_str
        
        # 検索文字列長
        self.find_str_len = len(find_str)
        
        # 検索文字列リスト
        self.find_str_list = list(find_str)
        
        # 検索文字列インデックス
        self.find_str_idx = 0
    
    # =========================
    # 文字列の中に検索文字列が存在するか判定
    # 
    # 検索文字列中の空白と改行は無視する
    # 前回判定時に途中までヒットしていた場合は検索を引き継ぐ
    # 
    # 例 検索文字列:広告をスキップ
    #   検索             find_idx
    #   回数 文字列      開始時 終了時
    #   1    あいうえお  0      0
    #   2    広告        0      2
    #   3    かきくけこ  2      0
    #   4    広告を      0      3
    #   5    スキッ      3      6
    #   5    プ          6      7      → ★ヒット(return True)
    # =========================
    def is_exist_find_str(self, str):
        # 空白と改行を削除
        str = str.strip()
        
        str_list = list(str)
        
        for str in str_list:
            # 検索する1文字を設定
            find_str_one = self.find_str_list[ self.find_str_idx ]
            
            if find_str_one == str:
                # 検索する1文字と合致した場合
                
                # インデックスを進める
                self.find_str_idx = self.find_str_idx + 1
                
                # 発見した場合はインデックスをリセットしてTrueを返却
                if self.find_str_len == self.find_str_idx:
                    self.find_str_idx = 0
                    return True
            else:
                # 検索する1文字と合致しなかった場合
                
                # インデックスをリセット
                self.find_str_idx = 0
        
        return False

# ==================================================
# テキストが存在した場合クリックする
# ==================================================
def click_text(txt, min_interval_time, top, bottom, left, right):
    # 環境変数PATHにTesseract(OCRツール)のパスを通す
    os.environ["PATH"] += os.pathsep + os.path.dirname(os.path.abspath(__file__)) + os.sep + RESORSES_FOLDER_NAME
    
    # 最大監視時間の間繰り返す
    timer = TIMER(min_interval_time)
    while True:
        if setting_value.flg_stop:
            break
        
        if timer.can_exec():
            click_text_all(txt, top, bottom, left, right)
        
        time.sleep(1)

# ==================================================
# 全画面に指定されたテキストが存在した場合クリックする
# ==================================================
def click_text_all(txt, top, bottom, left, right):
    # マルチディスプレイに対応するため全画面をキャプチャする
    cap_tmp = get_capture(False)
    
    # 指定領域をトリミング
    cap_tmp = cap_tmp[top : bottom, left: right]
    
    cap = cv2pil(cap_tmp)
    if cap is None:
        return
    
    # テキスト部分の座標を取得
    xy = get_xy(txt, cap)
    
    # クリック
    click_xy(xy, left, top)

# ==================================================
# テキスト部分の座標を取得
# ==================================================
def get_xy(txt, cap):
    xy = XY(-1, -1)
    
    # =========================
    # OCR処理
    # =========================
    tools = pyocr.get_available_tools()
    
    if len(tools) == 0:
        wx.MessageBox('OCR tool is not installed on the terminal.\n端末にOCRツールがインストールされていません。', 'Error エラー')
        sys.exit(1)
    
    tool = tools[0]
    
    dst = tool.image_to_string(
        cap,
        lang='jpn',
        builder=pyocr.builders.WordBoxBuilder(tesseract_layout=6)
    )
    
    str_finder = STR_FINDER(txt)
    
    for d in dst:
        found_txt = d.content
        
        # 検索文字列を発見したらリターン
        # 複数個見つかることは考慮しない
        if str_finder.is_exist_find_str(found_txt):
            return XY(d.position[0][0], d.position[0][1])
    
    return XY(-1, -1)

# ==================================================
# クリック
# ==================================================
def click_xy(xy, base_x, base_y):
    if xy.x == -1 and xy.y == -1:
        return
    
    pyautogui.click(x = base_x + xy.x + 1, y = base_y + xy.y + 1)
    
    xy.reset()

#==================================================================
# デスクトップのキャプチャ取得（マルチモニタ対応）
# 参考URLのコードをほぼそのまま利用しています。
# 
# ref https://se.yuttar-ixm.com/multi-monitor-cap/
#==================================================================
def get_capture(flag_gray: bool = True):
    try:
        # デスクトップ全体サイズ取得
        vscreenwidth = win32api.GetSystemMetrics(win32con.SM_CXVIRTUALSCREEN)
        vscreenheigth = win32api.GetSystemMetrics(win32con.SM_CYVIRTUALSCREEN)
        vscreenx = win32api.GetSystemMetrics(win32con.SM_XVIRTUALSCREEN)
        vscreeny = win32api.GetSystemMetrics(win32con.SM_YVIRTUALSCREEN)
        width = vscreenx + vscreenwidth
        height = vscreeny + vscreenheigth
    
        # デスクトップのデバイスコンテキスト取得
        hwnd = win32gui.GetDesktopWindow() 
        windc = win32gui.GetWindowDC(hwnd)
        srcdc = win32ui.CreateDCFromHandle(windc)
        memdc = srcdc.CreateCompatibleDC()

        # デバイスコンテキストからピクセル情報コピー、bmp化
        bmp = win32ui.CreateBitmap()
        bmp.CreateCompatibleBitmap(srcdc, width, height)
        memdc.SelectObject(bmp)
        memdc.BitBlt((0, 0), (width, height), srcdc, (0, 0), win32con.SRCCOPY)

        # イメージ取得／調整
        img = np.frombuffer(bmp.GetBitmapBits(True), np.uint8).reshape(height, width, 4)
        if flag_gray is True :
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # 解放
        srcdc.DeleteDC()
        memdc.DeleteDC()
        win32gui.ReleaseDC(hwnd, windc)
        win32gui.DeleteObject(bmp.GetHandle())

        return img
    
    except Exception as err:
        # 取得失敗
        return None

#==================================================================
# cv2型データをImage型データに変換
# 参考URLのコードをほぼそのまま利用しています。
# 
# ref https://qiita.com/derodero24/items/f22c22b22451609908ee
#==================================================================
def cv2pil(image):
    new_image = image.copy()
    
    if new_image.ndim == 2:  # モノクロ
        pass
    elif new_image.shape[2] == 3:  # カラー
        new_image = cv2.cvtColor(new_image, cv2.COLOR_BGR2RGB)
    elif new_image.shape[2] == 4:  # 透過
        new_image = cv2.cvtColor(new_image, cv2.COLOR_BGRA2RGBA)
    
    new_image = Image.fromarray(new_image)
    
    return new_image





# ==================================================
# ==================================================
# 直接実行用の関数
# 
# 引数なし: GUIを起動
# 引数あり: 全画面を対象にGUIなしで起動
# 
# 引数1 検索テキスト
# 引数2 最小実行間隔(秒)
# 引数3 top
# 引数4 bottom
# 引数5 left
# 引数6 right
# 
# 呼び出し例1 : py auto_click.py
#   ⇒ GUIを起動
# 呼び出し例2 : py auto_click.py "広告をスキップ" 1 500 800 2300 2800
#   ⇒ 全画面を対象にGUIなしで起動
# ==================================================
# ==================================================
if __name__ == '__main__':
    # 引数取得
    args = sys.argv
    
    # 引数なしの場合はGUIを起動
    if 1 == len(args):
        try:
            application = wx.App()
            
            setting_value = SETTING_VALUE()
            
            window_main = WINDOW_MAIN(FLAME_TITLE, FLAME_WIDTH, FLAME_HEIGHT)
            window_main.show()
            
            application.MainLoop()
        except:
            wx.MessageBox(traceback.format_exc(), 'FatalError 致命的なエラー')
        
        sys.exit(0)
    
    # 引数チェック
    if 7 != len(args):
        print('引数を下記のように指定してください。')
        print('引数なし : GUIを起動')
        print('引数1 : 検索テキスト')
        print('引数2 : 最小実行間隔(秒)')
        print('引数3 : top')
        print('引数4 : bottom')
        print('引数5 : left')
        print('引数6 : right')
        
        sys.exit(1)
    
    try:
        # 引数設定
        txt = args[1]
        min_interval_time = int(args[2])
        top = int(args[3])
        bottom = int(args[4])
        left = int(args[5])
        right = int(args[6])
        
        # テキストが存在した場合クリックする
        click_text(txt, min_interval_time, top, bottom, left, right)
    except:
        wx.MessageBox(traceback.format_exc(), 'FatalError 致命的なエラー')
    
    sys.exit(0)

